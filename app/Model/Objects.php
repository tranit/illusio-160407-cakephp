<?php

App::uses('AppModel', 'Model');

/**
 * Artists Model
 *
 * @property Page $Page
 */
class Objects extends AppModel {

    public $actsAs = array(
        'Containable',
        'Media.Media'
    );

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Collection' => array(
            'className' => 'Collection',
            'foreignKey' => 'collection_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
