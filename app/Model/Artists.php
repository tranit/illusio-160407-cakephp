<?php

App::uses('AppModel', 'Model');

/**
 * Artists Model
 *
 * @property Page $Page
 */
class Artists extends AppModel {

    public $actsAs = array(
        'Containable',
        'Media.Media'
    );

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'firstname';


}
