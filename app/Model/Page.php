<?php

App::uses('AppModel', 'Model');

/**
 * Page Model
 *
 */
class Page extends AppModel {

    public $actsAs = array(
        'Containable',
        'Media.Media',
        'Translate' => array(
            'name' => '_name',
            'description' => '_description'
        )
    );
    public $displayField = 'name';

}
