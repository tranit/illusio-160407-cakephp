<?php

App::uses('AppModel', 'Model');

/**
 * Collection Model
 *
 * @property Page $Page
 */
class Collection extends AppModel {

    public $actsAs = array(
        'Containable',
        'Media.Media',
        'Translate' => array(
            'name' => '_name',
        )
    );

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Page' => array(
            'className' => 'Page',
            'foreignKey' => 'page_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
