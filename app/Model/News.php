<?php

App::uses('AppModel', 'Model');

/**
 * News Model
 *
 * @property News $news
 */
class News extends AppModel {

    public $actsAs = array(
        'Containable',
        'Media.Media'
    );

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';


}
