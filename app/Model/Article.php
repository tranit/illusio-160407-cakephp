<?php

App::uses('AppModel', 'Model');

/**
 * Article Model
 *
 * @property Page $Page
 */
class Article extends AppModel {

    public $actsAs = array(
        'Containable',
        'Media.Media'
    );

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Page' => array(
            'className' => 'Page',
            'foreignKey' => 'page_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
