<?php

App::uses('AppController', 'Controller');

/**
 * Artists Controller
 *
 * @property Artists $Artists
 */
class ArtistsController extends AppController {

    public function view($id = null) {
        $this->loadModel('Artists');
        $this->Artists->id = $id;
        if (!$this->Artists->exists()) {
            throw new NotFoundException(__('Invalid collection'));
        }
        $artist = $this->Artists->read(null, $id);
        $img = '';
        foreach($artist['Media'] as $media){
            $img = $media['file']; break;
        }
        $seo_meta = array(
            'title' => $artist['Artists']['firstname'] . " " .$artist['Artists']['lastname'],
            'url' => "http://".$_SERVER['HTTP_HOST']. "/artists/view/".$artist['Artists']['id'],
            'description' => strip_tags($artist['Artists']['biography']),
            'image' => "http://".$_SERVER['HTTP_HOST']."/img/". $img
        );

        $this->set('seo_meta', $seo_meta);
        $this->set('artist', $artist);
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Artists->recursive = 0;
        $this->set('artists', $this->paginate());

    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Artists->id = $id;
        if (!$this->Artists->exists()) {
            throw new NotFoundException(__('Invalid artists'));
        }
        $this->Artists->recursive = 1;
        $this->set('artists', $this->Artists->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        $this->helpers[] = 'Media.Uploader';
        if ($this->request->is('post')) {
            $this->loadModel('Artists');
            $this->Artists->create();
            if ($this->Artists->saveAssociated($this->request->data)) {
                $this->flash(__('Artists saved.'), array('action' => 'index'));
            } else {
                
            }
        }

        //debug($pages);
        //$this->set(compact('pages'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->helpers[] = 'Media.Uploader';
        $this->loadModel('Artists');
        $this->Artists->id = $id;
        if (!$this->Artists->exists()) {
            throw new NotFoundException(__('Invalid artists'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Artists->save($this->request->data)) {
                $this->Session->setFlash(__('The artists has been saved.', true));
                $this->redirect(array('action' => 'index'));
            } else {
                
            }
        } else {
            $this->request->data = $this->Artists->readAll();
        }
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->loadModel('Artists');
        $this->Artists->id = $id;
        if (!$this->Artists->exists()) {
            throw new NotFoundException(__('Invalid artists'));
        }
        if ($this->Artists->delete()) {
            $this->flash(__('Artists deleted'), array('action' => 'index'));
        }
        $this->flash(__('Artists was not deleted'), array('action' => 'index'));
        $this->redirect(array('action' => 'index'));
    }

}
