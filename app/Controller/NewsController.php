<?php

App::uses('AppController', 'Controller');

/**
 * News Controller
 *
 * @property News $News
 */
class NewsController extends AppController {

    public function index() {
        $this->loadModel('News');
        $this->set('news', $this->paginate());

    }

    public function view($id = null) {
        $this->loadModel('News');
        $this->News->id = $id;
        if (!$this->News->exists()) {
            throw new NotFoundException(__('Invalid news'));
        }
        $news = $this->News->read(null, $id);
        $img = '';
        foreach($news['Media'] as $media){
            $img = $media['file']; break;
        }
        $seo_meta = array(
            'title' => $news['News']['title'] ,
            'url' => "http://".$_SERVER['HTTP_HOST']. "/news/view/".$news['News']['id'],
            'description' => strip_tags($news['News']['description']),
            'image' => "http://".$_SERVER['HTTP_HOST']."/img/". $img
        );

        $this->set('seo_meta', $seo_meta);
        $this->set('news', $news);
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->News->recursive = 0;
        $this->set('news', $this->paginate());

    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->News->id = $id;
        if (!$this->News->exists()) {
            throw new NotFoundException(__('Invalid News'));
        }
        $this->News->recursive = 1;
        $this->set('artists', $this->News->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        $this->helpers[] = 'Media.Uploader';
        if ($this->request->is('post')) {
            $this->loadModel('News');
            $this->News->create();
            if ($this->News->saveAssociated($this->request->data)) {
                $this->flash(__('News saved.'), array('action' => 'index'));
            } else {
                
            }
        }

        //debug($pages);
        //$this->set(compact('pages'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->helpers[] = 'Media.Uploader';
        $this->loadModel('News');
        $this->News->id = $id;
        if (!$this->News->exists()) {
            throw new NotFoundException(__('Invalid artists'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->News->save($this->request->data)) {
                $this->Session->setFlash(__('The news has been saved.', true));
                $this->redirect(array('action' => 'index'));
            } else {
                
            }
        } else {
            $this->request->data = $this->News->readAll();
        }
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->loadModel('News');
        $this->News->id = $id;
        if (!$this->News->exists()) {
            throw new NotFoundException(__('Invalid artists'));
        }
        if ($this->News->delete()) {
            $this->flash(__('News deleted'), array('action' => 'index'));
        }
        $this->flash(__('News was not deleted'), array('action' => 'index'));
        $this->redirect(array('action' => 'index'));
    }

}
