<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

//    public function beforeFilter() {
//        parent::beforeFilter();
//        $this->Auth->allow('add', 'logout');
//        $this->Auth->allow('add');
//    }
//
//    public function login() {
//        if ($this->request->is('post')) {
//            if ($this->Auth->login()) {
//                $this->redirect($this->Auth->redirect('admin/pages/edit/5'));
//            } else {
//                $this->Session->setFlash(__('Invalid username or password, try again'));
//            }
//        }
//    }
//
//    public function logout() {
//        $this->redirect($this->Auth->logout());
//    }
//    
////    public function add() {
////        if ($this->request->is('post')) {
////            $this->User->create();
////            if ($this->User->save($this->request->data)) {
////                $this->Session->setFlash(__('The user has been saved'));
////                $this->redirect(array('action' => 'index'));
////            } else {
////                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
////            }
////        }
////    }
//
//    /**
//     * admin_index method
//     *
//     * @return void
//     */
//    public function admin_index() {
//        $this->User->recursive = 0;
//        $this->set('users', $this->paginate());
//    }
//
//    /**
//     * admin_view method
//     *
//     * @throws NotFoundException
//     * @param string $id
//     * @return void
//     */
//    public function admin_view($id = null) {
//        $this->User->id = $id;
//        if (!$this->User->exists()) {
//            throw new NotFoundException(__('Invalid user'));
//        }
//        $this->set('user', $this->User->read(null, $id));
//    }
//
//    /**
//     * admin_add method
//     *
//     * @return void
//     */
//    public function admin_add() {
//        if ($this->request->is('post')) {
//            $this->User->create();
//            if ($this->User->save($this->request->data)) {
//                $this->flash(__('User saved.'), array('action' => 'index'));
//            } else {
//                
//            }
//        }
//    }
//
//    /**
//     * admin_edit method
//     *
//     * @throws NotFoundException
//     * @param string $id
//     * @return void
//     */
//    public function admin_edit($id = null) {
//        $this->User->id = $id;
//        if (!$this->User->exists()) {
//            throw new NotFoundException(__('Invalid user'));
//        }
//        if ($this->request->is('post') || $this->request->is('put')) {
//            if ($this->User->save($this->request->data)) {
//                $this->flash(__('The user has been saved.'), array('action' => 'index'));
//            } else {
//                
//            }
//        } else {
//            $this->request->data = $this->User->read(null, $id);
//        }
//    }
//
//    /**
//     * admin_delete method
//     *
//     * @throws MethodNotAllowedException
//     * @throws NotFoundException
//     * @param string $id
//     * @return void
//     */
//    public function admin_delete($id = null) {
//        if (!$this->request->is('post')) {
//            throw new MethodNotAllowedException();
//        }
//        $this->User->id = $id;
//        if (!$this->User->exists()) {
//            throw new NotFoundException(__('Invalid user'));
//        }
//        if ($this->User->delete()) {
//            $this->flash(__('User deleted'), array('action' => 'index'));
//        }
//        $this->flash(__('User was not deleted'), array('action' => 'index'));
//        $this->redirect(array('action' => 'index'));
//    }

}
