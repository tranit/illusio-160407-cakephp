<?php

App::uses('AppController', 'Controller');

/**
 * Pages Controller
 *
 * @property Page $Page
 * @property Artists $Artists
 */
class PagesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Pages';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @return void
     */
    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->loadModel('Artists');
        $Artists =$this->Artists->find('all');
        if ($page == "expo"){
            $this->set('artist_page',$Artists);
        } else {
            $this->set('info_page', ClassRegistry::init('Page')->find('all', array(
                'conditions' => array('shortname' => $page),
                'fields' => array('name')
            )));
        }

        if ($subpage == 'contact') {
            $this->set(compact('page', 'path', 'subpage', 'title_for_layout'));
            $this->set('_serialize', array('page', 'path', 'subpage', 'title_for_layout'));
        } else {
            $this->set(compact('page', 'path', 'subpage', 'title_for_layout'));
            $this->render(implode('/', $path));
        }
    }

    /**
     * Displays a artists
     *
     * @param mixed What page to display
     * @return void
     */
    public function artists($id = null){
        die('1');
    }


//    public function view($id = null) {
//        $this->Page->id = $id;
//        if (!$this->Page->exists()) {
//            throw new NotFoundException(__('Invalid collection'));
//        }
//        $this->set('page', $this->Page->read(null, $id));
//    }

    public function admin_home() {
        
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Page->recursive = 0;
        $this->set('pages', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->helpers[] = 'Media.Uploader';
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            throw new NotFoundException(__('Invalid page'));
        }
        $this->set('page', $this->Page->readAll());
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Page->create();
            if ($this->Page->save($this->request->data)) {
                $this->flash(__('Page saved.'), array('action' => 'index'));
            } else {
                
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->helpers[] = 'Media.Uploader';
        //$this->Page->locale = Configure::read('Config.laanguages');
        $this->Page->id = $id;
        //$this->data = $this->Page->readAll();
        //debug($this->data);
        if (!$this->Page->exists()) {
            throw new NotFoundException(__('Invalid page'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Page->saveAssociated($this->request->data)) { // saveAssociated !! not save !!!
                $this->Session->setFlash(__('The page has been saved.', true));
                $this->redirect(array('action' => 'view', $id));
                //$this->redirect($refer); // redirect back to the calling page after save
                //$this->flash(__('The page has been saved.'), array('action' => 'view'));
            } else {
                $this->Session->setFlash(__('The data could not be saved. Please, try again.', true));
            }
        } else {
            if ($this->Page->field('editable') == 1) {
                $this->request->data = $this->Page->readAll();
            } else {
                throw new NotFoundException(__('Page not editable'));
            }
        }
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
//    public function admin_delete($id = null) {
//        if (!$this->request->is('post')) {
//            throw new MethodNotAllowedException();
//        }
//        $this->Page->id = $id;
//        if (!$this->Page->exists()) {
//            throw new NotFoundException(__('Invalid page'));
//        }
//        if ($this->Page->delete()) {
//            $this->flash(__('Page deleted'), array('action' => 'index'));
//        }
//        $this->flash(__('Page was not deleted'), array('action' => 'index'));
//        $this->redirect(array('action' => 'index'));
//    }
}
