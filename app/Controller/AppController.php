<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $helpers = array('Text', 'Form', 'Html', 'Session', 'App', 'Js', 'Usermgmt.UserAuth');
    public $components = array('Session', 'RequestHandler', 'Usermgmt.UserAuth');

    function _setErrorLayout() {
        if ($this->name == 'CakeError') {
            // Si on a une erreur, on utilise notre layout crée au début de l'article
            $this->layout = 'error';
        }
    }

    // On effectue un test avant de rendre la vue
    function beforeRender() {
        $this->_setErrorLayout();
    }

    function beforeFilter() {
        // User Plugin
        if ( ($this->params['controller'] != "artists" && $this->params['action'] != "view")
            && ($this->params['controller'] != "collection" && $this->params['action'] != "object")
            && ($this->params['controller'] != "news" && $this->params['action'] != "index")
            && ($this->params['controller'] != "news" && $this->params['action'] != "view")
        ){
            $this->userAuth();
        }
        // Language

        if (!$this->Session->read('User.language')) {
            $this->Session->write('User.language', Configure::read('Config.language'));
        }
        if (isset($this->params['language'])) {
            if (in_array($this->params['language'], Configure::read('Config.languages'))) {
                $this->Session->write('User.language', $this->params['language']);
            }
        }
        $lan = $this->Session->read('User.language');
        Configure::write('Config.language', $this->Session->read('User.language'));
        //$this->parmas['language'] = $this->Session->read('User.language');
        // Root page
        // Auth
//        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'admin' => false);
//        $this->Auth->loginError = 'Invalid username / password combination. Please try again.';
//        $this->Auth->logoutRedirect = '/pages/home';
//
//        $this->Auth->allow('display');
//        $this->Auth->authorize = 'controller';
//
//        if (!isset($this->request->params['prefix'])) {
//            $this->Auth->allow();
//        }
        // Layout

        if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin' && $this->params['admin'] == 1) {
            $this->layout = 'admin_default';
            $this->Session->write('Usefull.layout', 'admin_default');
        } elseif (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin' && $this->params['admin'] == 1) {
            $this->layout = false;
        } else {
            if (isset($this->request->params['pass']['0']) && $this->request->params['pass']['0'] == 'may') {
                $this->layout = 'may';
                $this->Session->write('Usefull.layout', 'default');
            } else {
                $this->layout = 'default';
                $this->Session->write('Usefull.layout', 'default');
            }
        }

        // Menu

        $this->set('menu_collections', ClassRegistry::init('Collection')->find('all', array(
                    'fields' => array('Collection.name')
                )));
        $this->set('menu_pages', ClassRegistry::init('Page')->find('all', array(
                    'fields' => array('name')
                )));
    }

    function isAuthorized() {
        return true;
    }

    private function userAuth() {
        $this->UserAuth->beforeFilter($this);
    }

}
