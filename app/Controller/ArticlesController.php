<?php

App::uses('AppController', 'Controller');

/**
 * Articles Controller
 *
 * @property Article $Article
 */
class ArticlesController extends AppController {

    public $paginate = array(
        'limit' => 6,
        'order' => array(
            'Article.date' => 'desc'
        )
    );
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $articles = $this->Article->recursive = 1;
        $this->set('articles', $this->paginate());
        //$articles = $this->Article->find('all');
        //$this->set('articles', $articles);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        $this->set('article', $this->Article->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Article->create();
            if ($this->Article->save($this->request->data)) {
                $this->flash(__('Article saved.'), array('action' => 'index'));
            } else {
                
            }
        }
        $pages = $this->Article->Page->find('list');
        $this->set(compact('pages'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Article->save($this->request->data)) {
                $this->flash(__('The article has been saved.'), array('action' => 'index'));
            } else {
                
            }
        } else {
            $this->request->data = $this->Article->read(null, $id);
        }
        $pages = $this->Article->Page->find('list');
        $this->set(compact('pages'));
    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        if ($this->Article->delete()) {
            $this->flash(__('Article deleted'), array('action' => 'index'));
        }
        $this->flash(__('Article was not deleted'), array('action' => 'index'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Article->recursive = 0;
        $this->set('articles', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        $this->set('article', $this->Article->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        $this->helpers[] = 'Media.Uploader';
        if ($this->request->is('post')) {
            $this->Article->create();
            if ($this->Article->save($this->request->data)) {
                $this->flash(__('Article saved.'), array('action' => 'index'));
            } else {
                
            }
        }
        //$pages = $this->Article->Page->find('list');
        //$this->set(compact('pages'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->helpers[] = 'Media.Uploader';
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Article->save($this->request->data)) {
                $this->Session->setFlash(__('The article has been saved.', true));
                $this->redirect(array('action' => 'index'));
            } else {
                
            }
        } else {
            $this->request->data = $this->Article->read(null, $id);
        }
        //$pages = $this->Article->Page->find('list');
        //$this->set(compact('pages'));
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        if ($this->Article->delete()) {
            $this->flash(__('Article deleted'), array('action' => 'index'));
        }
        $this->flash(__('Article was not deleted'), array('action' => 'index'));
        $this->redirect(array('action' => 'index'));
    }

}
