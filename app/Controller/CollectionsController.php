<?php

App::uses('AppController', 'Controller');

/**
 * Collections Controller
 *
 * @property Collection $Collection
 */
class CollectionsController extends AppController {
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => 25,
        'contain' => array('Objects')
    );

    /**
     * index method
     *
     * @return void
     */
//    public function index() {
////        $this->Collection->recursive = 0;
////        $this->set('collections', $this->paginate());
//        $d = $this->Collection->find('all');
//        $this->set('d', $d);
//    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->Collection->id = $id;
        if (!$this->Collection->exists()) {
            throw new NotFoundException(__('Invalid collection'));
        }
        $this->loadModel('Objects');
        /*
        $this->Paginator->settings = array(
            'conditions' => array('Objects.collection_id' => $id),
            'order' => array(
                'Objects.id' => 'desc'
            ),
            'limit' => 6
        );
        $Objects = $this->Paginator->paginate('Objects');
        */
        $Objects = $this->Objects->find('all', array(
            'conditions' => array('Objects.collection_id' => $id),
            'order' => array(
                'Objects.name' => 'asc'
            ),
        ));
        $this->set('objects', $Objects);
        $this->set('collection', $this->Collection->read(null, $id));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function object($Collection_id, $id = null) {
        $this->loadModel('Objects');
        $this->Objects->id = $id;
        if (!$this->Objects->exists()) {
            throw new NotFoundException(__('Invalid collection'));
        }
        $Objects = $this->Objects->read(null, $id);
        $page =  ClassRegistry::init('Page')->find('all', array(
            'fields' => array('name'),
            'conditions' => array('Page.id' => 2)
        ));
        if ($this->Session->read('User.language') == 'fre') {
            $i = 0;
        } elseif ($this->Session->read('User.language') == 'eng') {
            $i = 1;
        }
        $img = '';
        foreach($Objects['Media'] as $media){
            $img = $media['file']; break;
        }
        $seo_meta = array(
            'title' => $Objects['Objects']['name'] ,
            'url' => "http://".$_SERVER['HTTP_HOST']. "/collections/object/".$Objects['Objects']['id'],
            'description' => strip_tags($Objects['Objects']['materials']),
            'image' => "http://".$_SERVER['HTTP_HOST']."/img/". $img
        );

        $this->set('page_name', $page[0]['_name'][$i]['content']);
        $this->set('category_name', array('name' => $Objects['Collection']['name'],  'id' => $Objects['Collection']['id']));
        $this->set('object_name', $Objects['Objects']['name']);
        $this->set('seo_meta', $seo_meta);
        $this->set('objects', $Objects);
    }


    /**
     * add method
     *
     * @return void
     */
//    public function add() {
//        if ($this->request->is('post')) {
//            $this->Collection->create();
//            if ($this->Collection->save($this->request->data)) {
//                $this->flash(__('Collection saved.'), array('action' => 'index'));
//            } else {
//                
//            }
//        }
//        $pages = $this->Collection->Page->find('list');
//        $this->set(compact('pages'));
//    }
//
//    /**
//     * edit method
//     *
//     * @throws NotFoundException
//     * @param string $id
//     * @return void
//     */
//    public function edit($id = null) {
//        $this->Collection->locale = Configure::read('Config.languages');
//        if (isset($this->data)) {
//            debug($this->data);
//            $this->Collection->save($this->data);
//        }
//        if ($id != null) {
//            $this->Collection->id = $id;
//            $this->data = $this->Collection->readAll();
//            debug($this->data);
//        }
////        $this->Collection->id = $id;
////        if (!$this->Collection->exists()) {
////            throw new NotFoundException(__('Invalid collection'));
////        }
////        if ($this->request->is('post') || $this->request->is('put')) {
////            if ($this->Collection->save($this->request->data)) {
////                $this->flash(__('The collection has been saved.'), array('action' => 'index'));
////            } else {
////                
////            }
////        } else {
////            $this->request->data = $this->Collection->read(null, $id);
////        }
////        $pages = $this->Collection->Page->find('list');
////        $this->set(compact('pages'));
//    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
//    public function delete($id = null) {
//        if (!$this->request->is('post')) {
//            throw new MethodNotAllowedException();
//        }
//        $this->Collection->id = $id;
//        if (!$this->Collection->exists()) {
//            throw new NotFoundException(__('Invalid collection'));
//        }
//        if ($this->Collection->delete()) {
//            $this->flash(__('Collection deleted'), array('action' => 'index'));
//        }
//        $this->flash(__('Collection was not deleted'), array('action' => 'index'));
//        $this->redirect(array('action' => 'index'));
//    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Collection->recursive = 0;
        $this->set('collections', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Collection->id = $id;
        if (!$this->Collection->exists()) {
            throw new NotFoundException(__('Invalid collection'));
        }
        $this->Collection->recursive = 1;
        $this->set('collection', $this->Collection->read(null, $id));
    }

    /**
     * admin_sub method
     *
     * @throws NotFoundException
     * @return void
     */
    public function admin_sub($id = null) {
        $page = $this->request->params['named']['page'];
        $page_first = $page > 1 ? ($page - 1): 1;
        $this->set('page', $page);
        $this->set('page_first', $page_first);


        $this->loadModel('Objects');
        $this->Paginator->settings = array(
            'conditions' => array('Objects.collection_id' => $id),
            'order' => array('Objects.title DESC'),
            'limit' => 10
        );
        $Objects = $this->Paginator->paginate('Objects');
        $this->set('objects', $Objects);
        $this->Collection->id = $id;
        if (!$this->Collection->exists()) {
            throw new NotFoundException(__('Invalid Objects'));
        }

        $this->Collection->recursive = 1;
        $this->set('collection', $this->Collection->read(null, $id));

    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_addsub($id = null) {
        $this->helpers[] = 'Media.Uploader';
        $this->loadModel('Objects');
        $this->set('collection_id',$id);

        $Collection = $this->Collection->find('all');
        $array = array();
        foreach ($Collection as $collection){
            $array[$collection['Collection']['id']] = $collection[0]['Collection__i18n__name'];

        }
        $this->set('collection_array',$array);
        if ($this->request->is('post')) {
            $this->Objects->create();
            if ($this->Objects->saveAssociated($this->request->data)) {
                $this->flash(__('Objects saved.'), array('action' => 'sub/' . $id));
            } else {

            }
        }
    }

    public function admin_editsub($id = null) {
        $this->helpers[] = 'Media.Uploader';
        $this->loadModel('Objects');
        $this->Objects->id = $id;

        $Collection = $this->Collection->find('all');
        $array = array();
        foreach ($Collection as $collection){
            $array[$collection['Collection']['id']] = $collection[0]['Collection__i18n__name'];

        }
        $this->set('collection_array',$array);
        $this->set('collection_edit',$this->Objects->readAll());
        if (!$this->Objects->exists()) {
            throw new NotFoundException(__('Invalid objects'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Objects->save($this->request->data)) {
                $this->Session->setFlash(__('The objects has been saved.', true));
                $this->redirect(array('action' => 'sub/' . $this->request->data['Objects']['collection_id']));
            } else {

            }
        } else {
            $this->request->data = $this->Objects->readAll();
        }
    }

    public function admin_deletesub($id = null, $collection_id, $page = 1) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->loadModel('Objects');
        $this->Objects->id = $id;
        if (!$this->Objects->exists()) {
            throw new NotFoundException(__('Invalid collection'));
        }
        if ($this->Objects->delete()) {
            $this->flash(__('Objects deleted'), array('action' => 'index'));
        }
        $this->flash(__('Objects was not deleted'), array('action' => 'index'));
        $this->redirect(array('action' => 'sub/'.$collection_id . "/". $page));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        $this->helpers[] = 'Media.Uploader';
        if ($this->request->is('post')) {
            $this->Collection->create();
            if ($this->Collection->saveAssociated($this->request->data)) {
                $this->flash(__('Collection saved.'), array('action' => 'index'));
            } else {
                
            }
        }

        //debug($pages);
        //$this->set(compact('pages'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->helpers[] = 'Media.Uploader';
        $this->Collection->id = $id;
        if (!$this->Collection->exists()) {
            throw new NotFoundException(__('Invalid collection'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Collection->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('The collection has been saved.', true));
                $this->redirect(array('action' => 'index'));
            } else {
                
            }
        } else {
            $this->request->data = $this->Collection->readAll();
        }
        //$pages = $this->Collection->Page->find('list');
        //$this->set(compact('pages'));
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Collection->id = $id;
        if (!$this->Collection->exists()) {
            throw new NotFoundException(__('Invalid collection'));
        }
        if ($this->Collection->delete()) {
            $this->flash(__('Collection deleted'), array('action' => 'index'));
        }
        $this->flash(__('Collection was not deleted'), array('action' => 'index'));
        $this->redirect(array('action' => 'index'));
    }

}
