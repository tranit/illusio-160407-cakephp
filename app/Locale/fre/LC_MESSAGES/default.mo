��    d      <  �   \      �     �     �     �     �     �  	   �     �     �     �     	     	     +	     :	     M	     c	     s	     �	     �	  %   �	     �	     �	     �	     �	     
  
   
     '
     :
     L
     g
  	   s
     }
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  
   �
  	                  !     $     ,     <     O     \  '   i     �     �     �  
   �     �  
   �     �     �     �     �               "     4  	   =  	   G     Q     Z     _     l  y   �     �          
            '   1     Y     ^     e     �     �  /   �     �                     $     )     6     B     W     `     f     k     �     �  5  �     �     �          	          /     A     X     k     �     �     �     �     �     �     �            *   2     ]     e     �  !   �     �  
   �      �  "   �  &        @  	   L     V     [     c     v     �     �     �     �     �     �     �               '     /     7     :     N     `     v     �  $   �     �     �     �     �          "     9     B     F     U     d     {     �     �     �     �     �     �     �        n   '     �     �     �     �      �  <   �               "  "   @     c  ?   �  !   �  !   �          
               0     H     `     r          �     �     �         V   1   \   Y           :             ?      A   c   0   S           F   `   !           4       #       .   >      H   3   O                 @       &             L             Q   '   U   	      5          B      Z      
   ]       +       9   $   N       6   (                    b   J       E       -   7      ,   "   ;       /       T   X   [   d                  2   K       ^   =   C          8   R               *            G           %   I      a   D   P   W      <                    _       M   )    A password is required A username is required Actions Add Article Add Collection Add Photo Add User Admin Add Article Admin Add Collection Admin Add Page Admin Add Photo Admin Add User Admin Edit Article Admin Edit Collection Admin Edit Page Admin Edit Photo Admin Edit User Architecture d'intérieur Are you sure you want to delete # %s? Article Article deleted Article saved. Article was not deleted Articles Collection Collection deleted Collection saved. Collection was not deleted Collections Creations Date Delete Delete Article Delete Collection Delete Photo Delete User Description Edit Edit Article Edit Collection Edit Page Edit Photo Edit User Gallerie Gallery Id Idusers Invalid article Invalid collection Invalid page Invalid user Invalid username or password, try again List Articles List Collections List Creations List Pages List Photos List Users Login Name New Article New Articles New Collection New Creations New Gallery Photo New Page New Pages New Photo New User Page Page deleted Page was not deleted Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end} Pages Password Photo Photos Please enter a valid role Please enter your username and password Role Submit The article has been saved. The collection has been saved. The page has been saved. The user could not be saved. Please, try again. The user has been saved The user has been saved. Title Url User User deleted User saved. User was not deleted Username Users View Website Administration next previous Project-Id-Version: May
POT-Creation-Date: 2012-10-29 23:28+0000
PO-Revision-Date: 2012-11-12 23:06-0000
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n>1);
X-Generator: Poedit 1.5.4
Language: fre
 Un mot de passe est requis Un nom d'utilisateur est requis Actions Nouvel article Ajouter une collection Ajouter une photo Ajouter un utilisateur Ajouter un article Ajouter une collection Ajouter une page Ajouter une photo Ajouter un utilisateur Editer un article Editer un collection Editer la page Editer une photo Editer l'utilisateur Architecture d'intérieur Etes vous certain de vouloir effacer # %s? Article L'article a été supprimé L'article a été sauvegardé L'article n'a pas été supprimé Articles Collection La collection a été supprimée La collection a été sauvegardée La collection n'a pas été supprimée Collections Creations Date Effacer Effacer un article Effacer une collection Supprimer une pho Supprimer l'utilisateur Description Editer Editer un article Editer une collection Editer la page Editer une photo Editer l'utilisateur Galerie Galerie Id ID de l'utilisateur Article incorrect Collection incorrecte Mauvaise page Utilisateur incorrect Utilisateur ou mot de passe incorect Lister les articles Lister les collections Lister les creations Lister les pages Lister les photos Liste des utilisateurs Connexio Nom Nouvel article Nouvel article Ajouter une collection Nouvelles creations ajouter une galerie photo Nouvelle page Nouvelle page Nouvelle photo Nouvel utilisateur Page La pag a été supprimée La page n'a pas été supprimée Page {:page} sur {:pages}, {:current} page sur {:count, commençant avec la {:start}, terminant avec la {:end} Pages Mot de passe Photo Photos Veuillez entrer un role adéquat Veuillez entrer vote nom d'utilisateur et votre mot de passe Rôle Envoyer L'article a été sauvegardé La collection a été sauvegardée La page a été sauvegardée L'utilisateur n'a pas pu être sauvegardé. Veuillez réessayer L'utilisateur a été sauvegardé L'utilisateur a été sauvegardé Titre Url Utilisateur Utilisateur supprimé Utilisateur sauvegardé Utilisateur non trouvé Nom d'utilisateur Utilisateurs Voir Administration du site suivante précédente 