# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2012-11-09 02:10+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Plugin/Contactform/Controller/ContactformController.php:35
msgid "contact form request"
msgstr ""

#: Plugin/Contactform/Controller/ContactformController.php:37
#: Plugin/Contactform/View/Contactform/show.ctp:21
msgid "name"
msgstr ""

#: Plugin/Contactform/Controller/ContactformController.php:38
#: Plugin/Contactform/View/Contactform/show.ctp:22
msgid "email"
msgstr ""

#: Plugin/Contactform/Controller/ContactformController.php:39
#: Plugin/Contactform/View/Contactform/show.ctp:23
msgid "message"
msgstr ""

#: Plugin/Contactform/Controller/ContactformController.php:42
msgid "sent from"
msgstr ""

#: Plugin/Contactform/Controller/ContactformController.php:45
msgid "contact form was submitted successfully"
msgstr ""

#: Plugin/Contactform/Controller/ContactformController.php:53
msgid "contact form"
msgstr ""

#: Plugin/Contactform/Model/Contactform.php:43
msgid "please insert your name"
msgstr ""

#: Plugin/Contactform/Model/Contactform.php:44
msgid "please insert your email address"
msgstr ""

#: Plugin/Contactform/Model/Contactform.php:45
msgid "please enter your message"
msgstr ""

#: Plugin/Contactform/View/Contactform/show.ctp:25
msgid "submit"
msgstr ""

