$(document).ready(function(){
	// Je défini mes variables
	var currentPageA = document.getElementsByClassName('currentPage')[0].getElementsByTagName('a')[0].innerHTML;
		
	// Les FR et EN du menu
	if (currentPageA == "Accueil" || currentPageA == "Galerie" || currentPageA == "Artistes" || currentPageA == "Architecture d\'intérieur" || currentPageA == "Presse" || currentPageA == "Contact") {
		$('#logo_en').css('color','#7A5708 !important');
		$('#logo_fr').css('color','#000');
	}
	else if (currentPageA !== "Collection"){
		$('#logo_fr').css('color','#7A5708 !important');
		$('#logo_en').css('color','#000');
	}
	
	// Pour la Galerie et Architecture : pas de lien
	if (currentPageA == "Galerie" || currentPageA == "Gallery" || currentPageA == "Architecture d\'intérieur" || currentPageA == "Interior") {
	$('.aDemInfoFR').css('display','none');
	$('.aDemInfoEN').css('display','none');
	}
	
	// Page Artistes
	// Français
	if (currentPageA == "Artistes") {
		var getRsGCaption = document.getElementById("rsGCaption").innerHTML;
		var premierLien = $('<a>',{
		text: 'Demande d\'information',
		class: 'aDemInfoFR',
		href: 'mailTo:contact@galerie-may.fr?subject=Demande d\'information | ' + getRsGCaption
		}).appendTo('.royalSlider');
		$('.aDemInfoFR').click(function(){
		getRsGCaptionNew = document.getElementById("rsGCaption").innerHTML;
	  	$(premierLien).attr('href','mailTo:contact@galerie-may.fr?subject=Demande d\'information | ' + getRsGCaptionNew);
		});
		$('.aDemInfoEN').css('display','none');
	}
	// Anglais (s'affiche aussi dans galerie, archi et en double dans les collections EN)
	else if (currentPageA == "Artists") {
		var getRsGCaption = document.getElementById("rsGCaption").innerHTML;
		var premierLienEng = $('<a>',{
		text: 'Ask for information',
		class: 'aDemInfoEN',
		href: 'mailTo:contact@galerie-may.fr?subject=Ask for information | ' + getRsGCaption
		}).appendTo('.royalSlider');
		$('.aDemInfoEN').click(function(){
		getRsGCaptionNew = document.getElementById("rsGCaption").innerHTML;
	  	$(premierLienEng).attr('href','mailTo:contact@galerie-may.fr?subject=Ask for information | ' + getRsGCaptionNew);
		});
	}
	
	// Toute la Collection
	// Français
	var currentCollectionA = document.getElementsByClassName('currentCollection')[0].getElementsByTagName('a')[0].innerHTML;
	if (currentCollectionA == "Assises" || currentCollectionA == "Tables" || currentCollectionA == "Rangements" || currentCollectionA == "Luminaires" || currentCollectionA == "Miroirs" || currentCollectionA == "Antiquités") {
		$('#logo_en').css('color','#7A5708 !important');
		$('#logo_fr').css('color','#000');
		var getRsGCaption = document.getElementById("rsGCaption").innerHTML;
		var premierLien = $('<a>',{
		text: 'Demande d\'information',
		class: 'aDemInfoFR',
		href: 'mailTo:contact@galerie-may.fr?subject=Demande d\'information | ' + getRsGCaption
		}).appendTo('.royalSlider');
		$('.aDemInfoFR').click(function(){
		getRsGCaptionNew = document.getElementById("rsGCaption").innerHTML;
	  	$(premierLien).attr('href','mailTo:contact@galerie-may.fr?subject=Demande d\'information | ' + getRsGCaptionNew);
		});
		$('.aDemInfoEN').css('display','none');
	}
	// Anglais
	else if (currentCollectionA == "Seats" || currentCollectionA == "Tables." || currentCollectionA == "Storage" || currentCollectionA == "Lamps" || currentCollectionA == "Mirrors" || currentCollectionA == "Antiquities") {
		$('#logo_fr').css('color','#7A5708 !important');
		$('#logo_en').css('color','#000');
		var getRsGCaption = document.getElementById("rsGCaption").innerHTML;
		var premierLienEng = $('<a>',{
		text: 'Ask for information',
		class: 'aDemInfoEN',
		href: 'mailTo:contact@galerie-may.fr?subject=Ask for information | ' + getRsGCaption
		}).appendTo('.royalSlider');
		$('.aDemInfoEN').click(function(){
		getRsGCaptionNew = document.getElementById("rsGCaption").innerHTML;
	  	$(premierLienEng).attr('href','mailTo:contact@galerie-may.fr?subject=Ask for information | ' + getRsGCaptionNew);
		});
		$('.aDemInfoFR').css('display','none');
	}
	
	// Onglet Tables
	

});