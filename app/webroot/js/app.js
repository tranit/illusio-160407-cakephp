/**
 * Menu
 */
function Menu() {
    $("#nav li ul").hide(); 
    
    $("#menu li").hover(
        function () {
            $(this).children("ul").show();
        },function(){
            $(this).children("ul").hide();
        });//hover
}

/**
 * Contact ajax request
 */
function imageScroller() {
    $( "#scroller" ).imageScroller( {
        loading:'Wait please...', 
        speed:'8000', 
        direction:'left'
    } );
}

/**
 * articleViewer
 */
function articleViewer() {
    if($("#articles").length > 0) {
        for(i=0;i<6;i++) {
            $(".article-"+i).colorbox({
                transition: 'elastic',
                rel: 'article-'+i,
                data: true,
                scalePhotos: true,
                maxHeight: '96%',
                opacity: 0.7
            });
        }
    }
}

function FirstPage() {
    if($(".lang_main").length > 0) {
        $("#content").prepend("<img id=\"logo_big\" src=\"/may/img/may_big6.png\">"); // best : 4
        $("#logo").hide();
    }
}

var pathNam = window.location.pathname;    
function Slideshow() {
    if($(".royalSlider").length > 0 && pathNam !==  '/maaaaaay/collections/view/10/language:' && pathNam !==  '/collections/view/10/language:') {
        $(".royalSlider").royalSlider({
            keyboardNavEnabled: true,
            loop: true,
            arrowsNavAutoHide: false,
            sliderTouch: false,
            transitionType: 'fade',
            controlNavigation: 'thumbs',
            imageScaleMode: 'fit',
            globalCaption:true
        
        });
    }
}

function blackToBottom() {
    if($('#under_bg').height() < $('#container').height()) {
        console.log($('#under_bg').height());
        console.log($('#container').height());
        var h = document.getElementById('container').clientHeight;
        console.log(h);
        console.log($(document).height());
        console.log($(window).height());



        $('#under_bg').height($(document).height());
        console.log($('#under_bg').height());
        console.log($('#container').height());

    }
}

jQuery(document).ready(function () {
    //FirstPage();
    Menu();
    imageScroller();
    Slideshow();
    articleViewer();
    blackToBottom();
});