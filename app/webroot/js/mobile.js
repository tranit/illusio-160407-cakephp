$(document).ready(function(){
	
	// Préhome en fonction de l'orientation du mobile
	// Si on est en horizontal
	if(window.orientation !== 0){
	    $('#container_container').css({'background':'url("/img/bgg8-mobile.png") no-repeat 44% 30% !important'});
	    $('#may_img').css({'padding': '16% 0 0 5%'});
	  }
	  
	// idem au changement d'orientation
	$(window).on("orientationchange",function(){
	  if(window.orientation !== 0){
	    $('#container_container').css({'background':'url("/img/bgg8-mobile.png") no-repeat 44% 30% !important'});
	    $('#may_img').css({'padding': '16% 0 0 5%'});
	  }
	});


	// on calcule la hauteur de la page et on l'applique à #nav
	var docHeight = $(document).height();
	$('#nav').css('height', docHeight + 'px');
	
	// Le menu mobile
	$('#menuMobileBurger').click(function(){
		$('#menuMobileBurger').toggleClass('menuMobileClicked');
		$('#menuMobileBurger span').toggleClass('menuMobileClickedSpan');
		$('#nav').slideToggle();
		$('#switch_language').slideToggle();
		
		// on calcule la hauteur de la page et on l'applique à #nav
		var docHeight = $(document).height();
		$('#nav').css('height', docHeight + 'px');
		
	});
	
	
	// Titre de la page : Catégorie
	$('#menu li').each(function( i ){
		if ($(this).hasClass('currentPage')){
			var currentPageTitle = $(this).children('a').text();
			
			// on rempli le titre
			$('.currentPageTitle').text(currentPageTitle);
			
			// Titre de la page : Sous-catégorie
			$('.submenu li').each(function( i ){
				if ($(this).hasClass('currentCollection')){
					var currentCollectionTitle = $(this).children('a').text();
					
					// on rajouter la sous-catégorie
					$('.currentPageTitle').text(currentPageTitle + ' | ' + currentCollectionTitle);
				}
			});
			
		}
	});

	
	// Hauteur des sliders au chargement de la page
	setTimeout(function(){
		$('.rsSlide').each(function( i ) {
		  if ($(this).css('display').toLowerCase() == 'block'){
		  	var rsImgHeight = $(this).children('.rsImg').height();
		  	var rsOverflowHeight = rsImgHeight + 20;
		  	var royalSliderHeight = rsImgHeight + 50;
		  	$('.rsOverflow').css('height', rsOverflowHeight + 'px');
		  	$('.royalSlider').css('height', royalSliderHeight + 'px');
		  }
		});
	}, 300);
	
	
	// Hauteur des sliders au changement d'orientation
	$( window ).on( "orientationchange", function( event ) {
		setTimeout(function(){
			$('.rsSlide').each(function( i ) {
			  if ($(this).css('display').toLowerCase() == 'block'){
			  	var rsImgHeight = $(this).children('.rsImg').height();
			  	var rsOverflowHeight = rsImgHeight + 20;
			  	var royalSliderHeight = rsImgHeight + 50;
		  		$('.rsOverflow').css('height', rsOverflowHeight + 'px');
		  		$('.royalSlider').css('height', royalSliderHeight + 'px');
			  }
			});
		}, 10);
	});
	
	
	// la hauteur des sliders au click sur les flèches
	$('.rsArrowIcn').click(function(){
		setTimeout(function(){
			$('.rsSlide').each(function( i ) {
			  if ($(this).css('display').toLowerCase() == 'block'){
			  	var rsImgHeight = $(this).children('.rsImg').height();
			  	var rsOverflowHeight = rsImgHeight + 20;
			  	var royalSliderHeight = rsImgHeight + 50;
		  		$('.rsOverflow').css('height', rsOverflowHeight + 'px');
		  		$('.royalSlider').css('height', royalSliderHeight + 'px');
			  }
			});
		}, 30);
	});
	
	
	// la hauteur des sliders au click sur les images
	$('.rsSlide').click(function(){
		setTimeout(function(){
			$('.rsSlide').each(function( i ) {
			  if ($(this).css('display').toLowerCase() == 'block'){
			  	var rsImgHeight = $(this).children('.rsImg').height();
			  	var rsOverflowHeight = rsImgHeight + 20;
			  	var royalSliderHeight = rsImgHeight + 50;
		  		$('.rsOverflow').css('height', rsOverflowHeight + 'px');
		  		$('.royalSlider').css('height', royalSliderHeight + 'px');
			  }
			});
		}, 30);
	});
	
	
	// Page Contact : correction de l'image
	var pathNam = window.location.pathname;
	if (pathNam == '/pages/contact/language:eng' || pathNam == '/pages/contact/' || pathNam == '/pages/contact'){
		$('.pageTitleWrapper').css('border-top', '1px solid #7A5708');
		$('#content img').css('margin-top','-168px');
	}
	
		
// Fin de document ready
});