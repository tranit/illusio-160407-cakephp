// Collection > Catalogues : affichage en mosaïques

$(document).ready(function(){
	var pathNam = window.location.pathname;
	if (pathNam == '/collections/view/10/language:' || pathNam ==  '/maaaaaay/collections/view/10/language:'){
		$('.royalSlider').css({
			"width": "100%",
			"height": "100%",
			"margin-top": "50px"
		});

		$('.royalSlider .rsImg').css({
			"display":"none"
		});

		// faire un each dans les .rsSlide img pour avoir le attr "src" et "alt"
		$('.royalSlider img').each(function(){
			var imgSrc = $(this).attr('src');
			var imgAlt = $(this).attr('alt');
			$(this).parent('.royalSlider').append("<a class='rsALink' href='" + imgSrc + "' target='_blank'>" + imgAlt + "</a>");
		});


		$('.royalSlider .rsALink').css({
			"position": "relative",
			"display": "block",
			"background": "url('/img/img_pdf.jpg') no-repeat center center",
			"background-size": "130px",
			"width": "130px",
			"height": "53px",
			"float": "left",
			"display": "block",
			"opacity":"1",
			"margin": "0 20px 20px 0",
			"padding": "120px 0 0 0",
			"color": "#7A5708",
    		"text-decoration": "none",
    		"text-align": "center",
    		"word-spacing": "50px"
		});

		
	}

});