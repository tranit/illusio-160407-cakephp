<script>
    $.ajax({
        type: 'GET',
        url: '/may/pages/contact/',
        data: { postVar1: 'description'},
        success:function(data){
            $('#contact_infos').append(data);
            //alert(data);
        }
    });
</script>
<div id="contact_infos">

</div>
<?php
echo $this->Html->css(array(
    '/Contactform/css/Contactform.css'
));
echo $this->Form->create('Contactform.Contactform');

echo $this->Form->input('Contactform.Name', array('label' => __d('contactform', 'name')));
echo $this->Form->input('Contactform.Mail', array('label' => __d('contactform', 'email')));
echo $this->Form->input('Contactform.Message', array('type' => 'textarea', 'label' => __d('contactform', 'message')));

echo $this->Form->submit(__d('contactform', 'submit'));

echo $this->Form->end();
?>