<?php
class MediasController extends AppController{
    
    public $components = array('Media.Img');
    public $order = array('Media.position ASC'); 

    function beforeFilter(){
        parent::beforeFilter(); 
        if(in_array($this->request->action, array('admin_upload','admin_index','admin_delete')) && array_key_exists('Security', $this->components)){
            $this->Security->validatePost = false;
            $this->Security->csrfCheck = false;
        }
        $this->layout = 'uploader';
    }

    function blocked(){
        throw new NotFoundException(); 
    }

    /**
    * Permet de cropper les images
    **/
    function crop(){ 
        if(!isset( $this->request->params['file'])){
            die(); 
        }
        extract($this->request->params);
        $file = str_replace('.','',$file); 
        $size = explode('x',$format); 
        $images = glob(IMAGES.$file.'.*');
        $dest = IMAGES.$file.'_'.$format.'.jpg';
        if(empty($images)){
            die(); 
        }else{
            $image = current($images); 
        }
        if($this->Img->redim($image,$dest,$size[0],$size[1])){
            header("Content-type: image/jpg");
            echo file_get_contents($dest);
            exit();
        }
    }

    function grayscale(){
        if(!isset( $this->request->params['file'])){
            die(); 
        }
        extract($this->request->params);
        $file = str_replace('.','',$file); 
        $dest = IMAGES.$file.'_bw.jpg';
        $file = IMAGES.$file.'.jpg';
        if(file_exists($file)){
            $img = imagecreatefromjpeg($file);
            imagefilter($img,IMG_FILTER_GRAYSCALE);
            imagejpeg($img,$dest,90);
            header("Content-type: image/jpg");
            echo file_get_contents($dest);
            exit();
        }
        die(); 
    }

    /**
    * Liste les médias
    **/
    function admin_index($ref,$ref_id, $page = null, $parent_id = null){
        $this->loadModel($ref); 
        $d['ref'] = $ref;
        $d['ref_id'] = $ref_id;
        if ($page != null) {
            if ($page == "Objects") {
                $medias = $this->Media->find('all', array(
                    'conditions' => array(
                        'ref_id' => $ref_id,
                        'ref' => $ref,
                        'obj_id' => $parent_id
                    )
                ));
            }
        } else {
            $medias = $this->Media->find('all', array(
                'conditions' => array('ref_id' => $ref_id, 'ref' => $ref)
            ));
        }
        $d['parent_id'] = $parent_id;
        $d['medias'] = $medias;
        $d['tinymce']= isset($this->request->params['named']['tinymce']);
        $d['thumbID'] = false;
        if($this->$ref->hasField('media_id')){
            $this->$ref->id = $ref_id; 
            $d['thumbID'] = $this->$ref->field('media_id');
        }
        $this->set($d);
    }

    function check_simple_file($medias , $file){
        $arr_file_upload = explode(".", $file['name']);
        if (count($medias) == 0){
            return 1;
        }
        elseif (!empty($medias) && count($medias) == 1 ){
            $arr = explode(".", $medias[0]['Media']['file']);
            if (strtolower(end($arr)) == 'pdf' && strtolower(end($arr_file_upload)) == "pdf"){
                // ton tai 1 file pdf
                return 2;
            }elseif((strtolower(end($arr)) == 'pdf' && strtolower(end($arr_file_upload)) != "pdf") || (strtolower(end($arr)) != 'pdf' && strtolower(end($arr_file_upload)) == "pdf") ){
               return 1;
            }
        } elseif (count($medias) == 2){
            $arr1 = explode(".", $medias[0]['Media']['file']);
            $arr2 = explode(".", $medias[1]['Media']['file']);
            $file1 = strtolower(end($arr1));
            $file2 = strtolower(end($arr2));

            if ($file1 != "pdf" && $file2 != "pdf"){
                return 1;
            } elseif (($file1 == "pdf" && $file2 != 'pdf' ) || ($file1 != 'pdf' && $file2 == "pdf") ){
                return 3;
            }

        }elseif (!empty($medias)){
            if (strtolower(end($arr_file_upload)) == 'pdf'){
                // xoa file iamge
                return 3;
            } else {
                return 1;
            }
        }

        return 1;
    }

    /**
    * Upload (Ajax)
    **/
    function admin_upload($ref,$ref_id, $parent_id = null){
        if ($ref == "Artists"){
            $this->Media->save(array(
                'ref'    => $ref,
                'ref_id' => $ref_id,
                'artist_id' => $ref_id,
                'file'   => $_FILES['file']
            ));
        } elseif( $ref == "Collection"){
            $medias = $this->Media->find('all',  array(
                    'conditions' => array('Media.ref_id' => $ref_id, 'Media.ref' => $ref , 'obj_id' => $parent_id ))
            );
            $check = $this->check_simple_file($medias , $_FILES['file']);
            if ($check == 2){
                $this->layout = false;
                $render = $this->render('admin_error2');
                die($render);
            } elseif($check == 3) {
                $this->layout = false;
                $render = $this->render('admin_error3');
                die($render);
            } else {
                $this->Media->save(array(
                    'ref'    => $ref,
                    'ref_id' => $ref_id,
                    'obj_id' => $parent_id,
                    'file'   => $_FILES['file']
                ));
            }

        } elseif( $ref == "Article" || $ref == "News"){
            $medias = $this->Media->find('all',  array(
                'conditions' => array('Media.ref_id' => $ref_id, 'Media.ref' => $ref ))
            );
            $check = $this->check_simple_file($medias , $_FILES['file']);
             if ($check == 2){
                 $this->layout = false;
                 $render = $this->render('admin_error2');
                 die($render);
             } elseif($check == 3) {
                 $this->layout = false;
                 $render = $this->render('admin_error3');
                 die($render);
            } else {
                 $this->Media->save(array(
                     'ref'    => $ref,
                     'ref_id' => $ref_id,
                     'file'   => $_FILES['file']
                 ));
             }

        }  else {
            $this->Media->save(array(
                'ref'    => $ref,
                'ref_id' => $ref_id,
                'file'   => $_FILES['file']
            ));
        }


        $this->loadModel($ref); 
        $d['v'] = current($this->Media->read());
        $d['tinymce']= isset($this->request->params['named']['tinymce']); 
        $d['thumbID'] = $this->$ref->hasField('media_id');
        $this->set($d);
        $this->layout = false; 
        $render = $this->render('admin_media'); 
        die($render);
    }

    /**
    * Suppression (Ajax)
    **/
    function admin_delete($id){
        $this->Media->delete($id); 
        die(); 
    }

    /**
    * Met l'image à la une
    **/
    function admin_thumb($id){
        $this->Media->id = $id; 
        $ref = $this->Media->field('ref');
        $ref_id = $this->Media->field('ref_id');
        $this->loadModel($ref);
        $this->$ref->id = $ref_id; 
        $this->$ref->saveField('media_id',$id);
        $this->redirect($this->referer());
    }

    function admin_order(){
        if(!empty($this->request->data['Media'])){
            foreach($this->request->data['Media'] as $k=>$v){
                $this->Media->id = $k;
                $this->Media->saveField('position',$v); 
            }
        }
        die(); 
    }
    

}