<div id="articles">
    <?php $i = 0;
	?>
    <?php foreach ($articles as $k => $v): ?>
        <?php if ($i % 3 == 0): ?>
            <div class="ligne">
            <?php endif; ?>
            <div class="article">
				<?php
				$temp = true;
				if (count($v['Media']) == 1){
					foreach ($v['Media'] as $media){
						$arr = explode(".", $media['file']);
						if (end($arr) == 'pdf'){
							echo "<a href='/img/" . $media['file'] . "'><img style='width:200px; height: 260px;' src='/img/img_pdf.jpg' /></a>";
							$temp = false;
						}
					}
				} elseif (count($v['Media']) == 2) {
					$img = "";
					$pdf = "";
					foreach ($v['Media'] as $media){
						$arr = explode(".", $media['file']);
						if (end($arr) == 'pdf'){
							$pdf = $media['file'];
							$temp = false;
						} else{
							$img =  $media['file'] ;
						}
					}
					if (!$temp ){
						echo "<a  target='_blank' href='/img/" . $pdf . "'><img style='width:200px; height: 260px;' src='/img/" . $img . "' /></a>";
					}
				}
				 ?>
			
			
                <?php 
				if ($temp):
					if (isset($v['Article']['thumb'])): ?>
						<?php
						echo $this->Html->link(
								$this->Html->image(
										sprintf($v['Article']['thumbf'], 200, 260)), '/img/' . sprintf($v['Article']['thumb'], 400, 600), array(
							'class' => 'article-' . $i,
							//'title' => $v['Article']['title'],
							'escape' => false
						));
						?>
						<?php $j = 1; ?>
						<?php foreach ($v['Media'] as $a => $b): ?>
							<?php if ($j != 1): ?>
								<?php echo $this->Html->link('', '/img/' . $b['file'], array('class' => 'article-' . $i)); ?>
							<?php endif; ?>
							<?php $j++; ?>
                    <?php endforeach; ?>
                    <?php $j = 1; ?>
                <?php endif;
				endif;
				?>
                <?php if ($i % 3 == 2): ?>
                </div>
            <?php endif; ?>
            <?php $i++; ?>
        </div>
    <?php endforeach; ?>
</div>

<div id="paginateur">
    <?php
    //echo $this->Paginator->first(__('first'));
    echo $this->Paginator->numbers(array(
        'class' => 'paginator',
        'currentClass' => 'paginatorCurrentPage',
        'separator' => ''
    ));
    //echo $this->Paginator->last(__('last'));
    ?>
</div>
<?php
//debug($articles);
?>