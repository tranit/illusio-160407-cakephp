<div class="articles form">
    <?php echo $this->Form->create('Article'); ?>
    <fieldset>
        <legend><?php echo __('Admin Add Article'); ?></legend>
        <?php
        echo $this->Form->input('title');
        echo $this->Form->input('date');
        //echo $this->Form->input('page_id');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>