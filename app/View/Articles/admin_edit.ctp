<div class="articles form">
    <?php echo $this->Form->create('Article'); ?>
    <fieldset>
        <legend><?php echo __('Admin Edit Article'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('title');
        echo $this->Form->input('date');
        ?><label for="mediasArticle">Photos :</label><?php
        echo $this->Uploader->iframe('Article', $this->request->data['Article']['id']);
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>