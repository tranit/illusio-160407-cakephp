<div id="admin_panel">
    <?php
    if ($this->Session->read('Usefull.layout', 'default') == 'default') {
        echo $this->Html->link(__('Administration'), '/admin/pages/edit/6', array('class' => 'button blue medium'));
    } else {
        echo $this->Html->link(__('Partie publique'), '/', array('class' => 'button green medium'));
    }
    echo ' ';
    echo $this->Html->link(__('LogOut'), '/logout', array('class' => 'button red medium'));
    ?>
</div>
<div id="admin_panel_fictive"></div>