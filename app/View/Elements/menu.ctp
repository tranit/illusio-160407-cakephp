<?php
if ($this->Session->read('User.language') == 'fre') {
$i = 0;
} elseif ($this->Session->read('User.language') == 'eng') {
$i = 1;
}
?>
<ul id="menu">
    <li class="<?php if (isset($this->params['pass']['0']) && $this->params['pass']['0'] == 'home' && $menu_pages['0']['Page']['id'] == 1) echo 'currentPage'; ?>">
        <?php echo $this->Html->link($menu_pages['5']['Page']['name'], array('plugin' => null, 'controller' => 'pages', 'action' => 'display', $menu_pages['5']['Page']['shortname'])); ?>
    </li>
    <li class="<?php if (isset($this->params['pass']['0']) && $this->params['pass']['0'] == 'galerie' && $menu_pages['0']['Page']['id'] == 1) echo 'currentPage'; ?>">
        <?php echo $this->Html->link($menu_pages['0']['Page']['name'], array('plugin' => null, 'controller' => 'pages', 'action' => 'display', $menu_pages['0']['Page']['shortname'])); ?>
    </li>
    <li class="<?php if (isset($this->params['pass']['0']) && $this->params['pass']['0'] == 'expo' && $menu_pages['0']['Page']['id'] == 1) echo 'currentPage'; ?>">
        <?php echo $this->Html->link($menu_pages['6']['Page']['name'], array('plugin' => null, 'controller' => 'pages', 'action' => 'display', $menu_pages['6']['Page']['shortname'])); ?>
    </li>
    <li class="<?php if (isset($this->params['controller']) && $this->params['controller'] == 'collections') {echo 'currentPage';} ?>">
        <a href="#"><?php echo $menu_pages['1']['Page']['name']; ?></a>
        <ul class="submenu">
            <?php foreach ($menu_collections as $c): ?>
                <li class="<?php if (isset($this->params['controller']) && $this->params['controller'] == 'collections' && $this->params['pass']['0'] == $c['Collection']['id']) echo 'currentCollection'; ?>">
                    <?php echo $this->Html->link($c['Collection']['name'], array('plugin' => null, 'language' => '', 'controller' => 'collections', 'action' => 'view', $c['Collection']['id'])); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>
    <li class="<?php if (isset($this->params['pass']['0']) && $this->params['pass']['0'] == 'architecture' && $menu_pages['0']['Page']['id'] == 1) echo 'currentPage'; ?>">
        <?php echo $this->Html->link($menu_pages['2']['Page']['name'], array('plugin' => null, 'controller' => 'pages', 'action' => 'display', $menu_pages['2']['Page']['shortname'])); ?>
    </li>
    <li class="<?php if (isset($this->params['controller']) && $this->params['controller'] == 'articles') echo 'currentPage'; ?>">
        <?php echo $this->Html->link($menu_pages['3']['Page']['name'], array('plugin' => null, 'controller' => 'articles', 'action' => 'index')); ?>
    </li>
     <li class="<?php if (isset($this->params['controller']) && $this->params['controller'] == 'news') echo 'currentPage'; ?>">
            <?php echo $this->Html->link($menu_pages['8']['Page']['name'], array('plugin' => null, 'controller' => 'news', 'action' => 'index')); ?>
        </li>
    <li class="<?php if (isset($this->params['pass']['0']) && $this->params['pass']['0'] == 'contact' && $menu_pages['0']['Page']['id'] == 1) echo 'currentPage'; ?>">
        <?php echo $this->Html->link($menu_pages['4']['Page']['name'], array('plugin' => null, 'controller' => 'pages', 'action' => 'display', $menu_pages['4']['Page']['shortname'])); ?>
    </li>
</ul>
