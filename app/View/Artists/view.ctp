
<div class="detail-artist">
	<div class="image">
		<?php $img = ''; foreach($artist['Media'] as $media): ?>
			<?php $img = $media['file']; echo $this->Html->image($media['file'], array('class' => 'rsImg', 'alt' => '')); ?>
		<?php endforeach; ?>
	</div>
	<div class="description">
		<h3><?php echo __('Artist'); ?> : <?php echo $artist['Artists']['firstname'] . " " .$artist['Artists']['lastname'] ;  ?></h3>
		<h3><?php echo __('Description'); ?> : </h3>
		<?php echo $artist['Artists']['biography']; ?>
		<div>
		<?php
			$title=$artist['Artists']['firstname'] . " " .$artist['Artists']['lastname'];
			$url="http://".$_SERVER['HTTP_HOST']. "/artists/view/".$artist['Artists']['id'];
			$summary=strip_tags($artist['Artists']['biography']);
			$image=$this->Html->image($img);
		?>
			 <p class="share_new  share_p">
				<a class="fb-share" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" target="_blank" title="Share on Facebook"></a>
				<a class="twitter-share" href="http://twitter.com/intent/tweet?text=<?php echo $url; ?>" target="_blank" title="Share on Twitter"></a>
				<a class="inn-share" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url; ?>" target="_blank" title="Share on LinkedIn"></a>
				 <a href="http://pinterest.com/pin/create/button/?url=<?php echo $seo_meta['url'] ?>&media=<?php echo $seo_meta['image'] ?>&description=<?php echo $seo_meta['description'] ?>" class="pin-it-button" count-layout="horizontal">
					<img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" height="17"/>
				</a>
			</p>
		</div>
	</div>
	<div class="clear"></div>
<div>