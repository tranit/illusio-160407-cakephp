<div class="artists view">
<h2><?php  echo __('Category Artists'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($artists['Collection']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prénom'); ?></dt>
		<dd>
			<?php echo h($artists['Artists']['firstname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom de famille'); ?></dt>
        		<dd>
        			<?php echo h($artists['Artists']['lastname']); ?>
        			&nbsp;
        		</dd>
		<dt><?php echo __('Page'); ?></dt>
		<dd>
			<?php echo $this->Html->link($artists['Page']['name'], array('controller' => 'pages', 'action' => 'view', $artists['Page']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Collection'), array('action' => 'edit', $artists['Artists']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Collection'), array('action' => 'delete', $artists['Artists']['id']), null, __('Are you sure you want to delete # %s?', $collection['Collection']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Collections'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Collection'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pages'), array('controller' => 'pages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Page'), array('controller' => 'pages', 'action' => 'add')); ?> </li>
	</ul>
</div>
