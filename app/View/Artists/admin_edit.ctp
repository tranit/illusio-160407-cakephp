<div class="collections form">
    <?php echo $this->Form->create('Artists'); ?>
    <fieldset>
        <legend><?php echo __('Admin Edit Artist'); ?></legend>
        <?php
        echo $this->Form->input('firstname' , array('label' => 'Prénom :'  , 'required' => 'required'));
        echo $this->Form->input('lastname' , array('label' => 'Nom de famille :'  , 'required' => 'required'));
        ?>
		<label for="ArtistBiography"><?php echo __('Biography'); ?> :</label>
		<?php echo $this->Uploader->tinymce('biography'); ?>
		<label for="mediasCollection">Photos :</label><?php
        echo $this->Uploader->iframe('Artists', $this->request->data['Artists']['id'])
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>

