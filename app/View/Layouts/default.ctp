<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-site-verification" content="Se6L0uvkewN2hPPLzaSuU0GUFxKEe5qen2om2eS0Uzo" />
        <title>
            <?php echo $cakeDescription ?>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array(
            'reset',
            'app',
            //'cake.generic',
            'admin_design',
            'animate.min',
            'colorbox',
            'royalslider',
            'rs-minimal-white',
            'http://fonts.googleapis.com/css?family=Open+Sans:300italic,300,600',
            'http://fonts.googleapis.com/css?family=Tenor+Sans',
            'fontBlair'
        ));
        echo $this->Html->script(array(
            'jquery-1.8.2.min',
            'modernizr',
            'jquery.imageScroller',
            'jquery.colorbox',
            'Jquery.royalslider.min',
            'jquery.easing.min',
            'app',
            'catalogues',
        ));

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>

        <?php if (!$this->request->is('mobile')): ?>
            <script src="/js/demInfo.js" ></script>
        <?php endif; ?>
        <?php if ($this->request->is('mobile')): ?>
            <link rel="stylesheet" type="text/css" href="/css/mobile.css">
            <script src="/js/mobile.js" ></script>
            <script src="/js/demInfo.js" ></script>
        <?php endif; ?>
        <?php if (isset($seo_meta)) :
        ?>
            <meta property="og:title" content="<?php echo $seo_meta['title'];  ?>" />
            <meta property="og:description" content="<?php echo $seo_meta['description'];  ?>" />
            <meta property="og:image" content="<?php echo $seo_meta['image']; ?>" />

            <meta property="og:url" content="<?php echo $seo_meta['url']; ?>" />
            <meta property="og:site_name" content="May" />
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="object" />
        <?php endif; ?>

    </head>
    <body>
        <?php if ($this->Session->read('UserAuth')): ?>
            <?php echo $this->element('admin_panel'); ?>
        <?php endif; ?>
        <!--        <div id="under_bg"><div class="clear"></div></div>-->
        <div id="container">
<!--            <img src="/may/img/bg_left.png" id="bg_left" />
            <img src="/may/img/bg_right.png" id="bg_right" />-->

            <div id="header">
            	<?php if ($this->request->is('mobile')): ?>
	            	<a id="menuMobileBurger">
	            		<span></span>
	            		<span></span>
	            		<span></span>
	            	</a>
		        <?php endif; ?>
                <div id="header_container">
                    <?php echo $this->Html->link($this->Html->image('may7.png'), array('plugin' => '', 'controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false, 'id' => 'logo', 'class' => 'column grid_3')); ?>
                    <div id="pictos">
                    	<a href="http://www.facebook.com/GalerieMay" target="_blank" id="picto-fb"><img src="http://www.galerie-may.fr/app/webroot/img/picto_may_fb.png"></a>
                    	<a href="http://instagram.com/galerie_may" target="_blank" id="picto-insta"><img src="http://www.galerie-may.fr/app/webroot/img/picto_may_insta.png"></a>
                    	<a href="https://www.pinterest.com/maylisq" target="_blank" id="picto-pin"><img src="http://www.galerie-may.fr/app/webroot/img/picto_may_pin.png"></a>
                    </div>
                    <div id="nav">
                        <?php echo $this->element('menu'); ?>
                    </div>
                    <div id="switch_language">
                        <?php echo $this->element('switch_language'); ?>
                    </div>
                </div>
            </div>
            
            <?php if ($this->request->is('mobile')): ?>
            	<div class="pageTitleWrapper">
            		<p class="currentPageTitle"></p>
            		<span class="absShadow"></span>
            	</div>
            <?php endif; ?>
            
            <div id="content">
                <div id="flash" class="animated bounce"><?php echo $this->Session->flash(); ?><?php echo $this->Session->flash('contact'); ?></div>

                <?php echo $this->fetch('content'); ?>
            </div>
            <div class="clear"></div>
        </div>
        <?php if (isset($this->params['controller']) && $this->params['controller'] == 'articles'): ?>
            <div class="clear"></div>
        <?php endif; ?>
        <?php
//debug($this->params);
        echo $this->element('sql_dump');
        ?>
    </body>
</html>
