<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-site-verification" content="Se6L0uvkewN2hPPLzaSuU0GUFxKEe5qen2om2eS0Uzo" />
        <title>
            <?php echo $cakeDescription ?>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array(
            'reset',
            'admin_design'
        ));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <?php if ($this->request->is('mobile')): ?>
            <link rel="stylesheet" type="text/css" href="/css/mobile.css">
            <script src="/js/mobile.js" ></script>
        <?php endif; ?>
        <style type="text/css">
            html {
                height: 100%;
                margin: 0;
            }
            body {
                background-color: #fff;
                height: 100%;
            }
            #may_img{
                position: relative;
                top: 560px;
                left: 160px;
                width: 250px;
            } 
            #container {
                background: url('/img/bgg8.png') left no-repeat;
                background-color: #fff;
                height: 780px;
                position: relative;
                z-index: 10;
            }
            #container_container {
                min-height: 100%;
                width: 1080px;
                margin: 0 auto;
                position: relative;
                z-index: 8;
                background: url('/img/bgg8_bottom.png') repeat-y;
            }
            #bg_right {
                width: 50%;
                height: 780px;
                background: url('/img/bgg8_right.png') repeat-x;
                z-index: 1;
                float: right;
            }
        </style>
    </head>
    <body>

        <?php if ($this->Session->read('UserAuth')): ?>
            <?php echo $this->element('admin_panel'); ?>
        <?php endif; ?>

        <div id="bg_right"></div>

        <div id="container_container">
            <div id="container">
                <div id="may_img">
                    <?php echo $this->Html->link($this->Html->image('may_big8.png'), array('plugin' => '', 'controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false, 'id' => 'logo', 'class' => 'column grid_3')); ?>
                </div>
            </div>
        </div>

        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>
