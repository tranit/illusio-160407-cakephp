<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css(array(
            'reset',
            'cake.generic',
            '1044_12_10_10',
            '/usermgmt/css/umstyle',
            'admin_design',
            'animate.min',
            'colorbox',
            'http://fonts.googleapis.com/css?family=Open+Sans:300italic,300,600'
        ));

        echo $this->Html->script(array(
            'jquery-1.8.2.min',
            'jquery.imageScroller',
            'modernizr',
            'app'
        ));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
    </head>
    <body>


        <?php if ($this->Session->read('UserAuth')): ?>
            <?php echo $this->element('admin_panel'); ?>
        <?php endif; ?>

        <div id="container">
            <div id="content" class="row">

                <div id="flashContent" class="animated bounce"><?php echo $this->Session->flash(); ?></div>

                <?php if ($this->Session->read('UserAuth')): ?>
                    <div id="admin_menu" class="column grid_3">
                        <?php echo $this->element('admin_menu'); ?>
                    </div>
                <?php endif; ?>

                <div class="column <?php
                if (!$this->Session->read('UserAuth')) {
                    echo 'grid_12';
                } else {
                    echo 'grid_9';
                }
                ?>">
                         <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>

        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>
