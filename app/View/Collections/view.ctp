
<?php 
$temp = true;
if (!empty($objects)): ?>
	<div class="collection">
    <?php
    foreach ($objects as $object) {
    $pdf = '';
		if (!empty($object['Media'])):
		$temp = false;
		?>
			<div class="row-item">
				<?php
					$temp = true;
					if (count($object['Media']) == 1){
						foreach ($object['Media'] as $media){
							$arr = explode(".", $media['file']);
							if (end($arr) == 'pdf'){
								echo "<a href='/img/" . $media['file'] . "'><img style='width:200px; height: 260px;' src='/img/img_pdf.jpg' /></a>";
								$temp = false;
							}
						}
					} elseif (count($object['Media']) == 2) {
						$img = "";
						$pdf = "";
						foreach ($object['Media'] as $media){
							$arr = explode(".", $media['file']);
							if (end($arr) == 'pdf'){
								$pdf = $media['file'];
								$temp = false;
							} else{
								$img =  $media['file'] ;
							}
						}
						if (!$temp ){
							echo "<a  target='_blank' href='/img/" . $pdf . "'><img style='width:200px; height: 260px;' src='/img/" . $img . "' /></a>";
						}
					}
					 ?>


				<?php
				if ($temp):
					$tem = true;
					foreach($object['Media'] as $media){
						if($tem){
						 list($uploads, $year, $month, $name) = split('[/.-]', $media['file']);
                                $name = str_replace('_', ' ', $name);
							echo $this->Html->image($media['file'], array('class' => 'rsImg', 'alt' => $name));
							$tem = false;
						}
					}
					 ?>
				 <?php endif; ?>
				<h4><?php
						if($temp)
						 	echo $this->Html->link( $object['Objects']['name']  , array( 'controller' => 'collections','action' => 'object', $object['Collection']['id'], $object['Objects']['id']),array("class" => "title"));
						else
							echo "<a  class='title' target='_blank' href='/img/" . $pdf . "'>" . $object['Objects']['name'] . "</a>";
				  ?></h4>
				<div class="royalSlider">
					<div class="rsOverflow"></div>
				</div>

			</div>

		<?php
		endif;
		}
    ?>
	<div class="clear"></div>
<?php endif; ?>
