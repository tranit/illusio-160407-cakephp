<style>
	.menu-list li {
        display: inline-table;
        margin-right: 12px;
    }

    li.none{
		margin:0px;
		font-size:12px;
	}
	.menu-list li a{
		color: #7A5708;
		text-decoration: none;
	}
</style>
<div class="detail-artist">

	<div class="image">
		<?php foreach($objects['Media'] as $media): ?>
			<?php echo $this->Html->image($media['file'], array('class' => 'rsImg', 'alt' => '')); ?>
		<?php endforeach; ?>
	</div>
	<div class="description">
		<div class="menu-list">
    		<ul>
    			<li><?php echo $page_name; ?></li>
    			<li class="none">></li>
    			<li><?php echo $this->Html->link($category_name['name'], array('plugin' => null, 'language' => '', 'controller' => 'collections', 'action' => 'view', $category_name['id'])); ?></li>
    			<li class="none">></li>
    			<li><?php echo $object_name;  ?></li>
    		</ul>
    	</div>
		<h3><?php echo __('Name'); ?> : <?php echo $objects['Objects']['name'] ;  ?></h3>
		<h3><?php echo __('Designer'); ?> : <?php echo $objects['Objects']['designer']  ;  ?></h3>
		<h3><?php echo __('Description'); ?> : </h3>
		<?php echo $objects['Objects']['dimensions']; ?>
		<h3><?php echo __('Materials'); ?> : </h3>
		<?php echo $objects['Objects']['materials']; ?>
		<h3><?php echo __('Lead time'); ?> : <?php echo $objects['Objects']['leadtime']  ;  ?></h3>
	</div>
	<div class="clear"></div>
	<?php
			$url="http://".$_SERVER['HTTP_HOST']. "/collections/object/".$artist['Artists']['id'];
		?>
	 <p class="share_new  share_p">
		<a class="fb-share" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" target="_blank" title="Share on Facebook"></a>
		<a class="twitter-share" href="http://twitter.com/intent/tweet?text=<?php echo $url; ?>" target="_blank" title="Share on Twitter"></a>
		<a class="inn-share" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url; ?>" target="_blank" title="Share on LinkedIn"></a>
		 <a href="http://pinterest.com/pin/create/button/?url=<?php echo $seo_meta['url'] ?>&media=<?php echo $seo_meta['image'] ?>&description=<?php echo $seo_meta['description'] ?>" class="pin-it-button" count-layout="horizontal">
			<img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" width="17"/>
		</a>
	</p>
<div>