<div class="collections form">
    <?php echo $this->Form->create('Collection'); ?>
    <fieldset>
        <legend><?php echo __('Admin Add Collection'); ?></legend>
        <?php
        foreach (Configure::read('Config.languages') as $lang) {
            echo $this->Form->input('Collection.name.' . $lang, array('label' => 'Name (' . $lang . ') :'));
        }
        //echo $this->Form->input('page_id');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>