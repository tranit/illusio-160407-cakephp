<div class="collections form">
    <?php echo $this->Form->create('Objects'); ?>
    <fieldset>
        <legend><?php echo __('Admin Edit Objects Collection'); ?></legend>
		 <?php
			echo $this->Form->input(
				'collection_id',
				array('options' => $collection_array, 'default' => $collection_edit['Objects']['collection_id'])
			);
		 ?>
		 
		 <?php // echo $this->Form->hidden('collection_id')?>
		 <?php
        echo $this->Form->input('name' , array('label' => 'Nom :' , 'required' => 'required'));
        echo $this->Form->input('designer' , array('label' => 'Concepteur :'  , 'required' => 'required'));
        echo $this->Form->input('leadtime' , array('label' => 'Délai :'));
        ?>
		<label for="ArtistBiography"><?php echo __('Dimensions'); ?> :</label>
		<?php echo $this->Uploader->tinymce('dimensions'); ?>
		<label for="ArtistBiography"><?php echo __('matériels'); ?> :</label>
		<?php echo $this->Uploader->tinymce('materials'); ?>
		<label for="mediasCollection">Photos :</label><?php
		echo $this->Uploader->iframe('Collection', $this->request->data['Collection']['id'].'/Objects/'. $this->request->data['Objects']['id'])
       ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>