<div class="collections index">
    <h2><?php echo __('Catégorie Collections'); ?></h2>
    <div class="actions">
        <h3><?php echo __('Actions');
         ?></h3>
        <ul>
            <li><?php echo $this->Html->link(__('New Catégorie Collections'), array('action' => 'addsub' )); ?></li>
        </ul>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('designer', 'Concepteur'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php  $temp = true; foreach ($objects as $object): ?>
            <tr>
                <td><?php echo h($object['Objects']['id']); ?>&nbsp;</td>
                <td><?php echo strtoupper(h($object['Objects']['name'])); ?>&nbsp;</td>
                <td><?php echo h($object['Objects']['designer']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'editsub', $object['Objects']['id'])); ?>
                    <?php if($temp && count($objects) == 1){
                        $temp = false;
                      echo $this->Form->postLink(__('Delete'), array('action' => 'deletesub', $object['Objects']['id'] , $object['Collection']['id'], $page_first ), null, __('Are you sure you want to delete # %s?', $object['Objects']['id']));
                      }
                     else
                        echo $this->Form->postLink(__('Delete'), array('action' => 'deletesub', $object['Objects']['id'] ,  $object['Collection']['id'], $page ), null, __('Are you sure you want to delete # %s?', $object['Objects']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>

    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
