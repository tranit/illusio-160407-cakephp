<div class="collections form">

    <?php //echo debug($this->request->data); ?>

    <?php echo $this->Form->create('Collection'); ?>
    <fieldset>
        <legend><?php echo __('Admin Edit Collection'); ?></legend>
        <?php
        foreach (Configure::read('Config.languages') as $lang) {
            echo $this->Form->input('id');
            echo $this->Form->input('Collection.name.' . $lang, array('label' => 'Name (' . $lang . ') :'));
        }
        ?><label for="mediasCollection">Photos :</label><?php
        echo $this->Uploader->iframe('Collection', $this->request->data['Collection']['id'])
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>

