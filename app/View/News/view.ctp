

<div class="detail-artist">
	<div class="image">
	
	<?php
	$temp = true;
	if (count($news['Media']) == 1){
		foreach ($news['Media'] as $media){
			$arr = explode(".", $media['file']);
			if (end($arr) == 'pdf'){
				echo "<a href='/img/" . $media['file'] . "'><img src='/img/img_pdf.jpg' /></a>";
				$temp = false;
			}
		}
	} elseif (count($news['Media']) == 2) {
		$img = "";
		$pdf = "";
		foreach ($news['Media'] as $media){
			$arr = explode(".", $media['file']);
			if (end($arr) == 'pdf'){
				$pdf = $media['file'];
				$temp = false;
			} else{
				$img =  $media['file'] ;
			}
		}
		if (!$temp ){
			echo "<a  target='_blank' href='/img/" . $pdf . "'><img src='/img/" . $img . "' /></a>";
		}
	}
	 ?>
	
		<?php $img = ''; 
		if ($temp):
			foreach($news['Media'] as $media): ?>
				<?php $img = $media['file']; echo $this->Html->image($media['file'], array('class' => 'rsImg', 'alt' => '')); ?>
			<?php endforeach;
		endif;
		?>
	</div>
	<div class="description detail-new">
		<h3><?php echo $news['News']['title'] ?> </h3>
		<?php echo $news['News']['description'] ?>
		<div>
		<?php
			
			$url="http://".$_SERVER['HTTP_HOST']. "/news/view/".$news['News']['id'];
			$summary=strip_tags($news['News']['description']);
			$image=$this->Html->image($img);
		?>
			 <p class="share_new  share_p">
				<a class="fb-share" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" target="_blank" title="Share on Facebook"></a>
				<a class="twitter-share" href="http://twitter.com/intent/tweet?text=<?php echo $url; ?>" target="_blank" title="Share on Twitter"></a>
				<a class="inn-share" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url; ?>" target="_blank" title="Share on LinkedIn"></a>
				 <a href="http://pinterest.com/pin/create/button/?url=<?php echo $seo_meta['url'] ?>&media=<?php echo $seo_meta['image'] ?>&description=<?php echo $seo_meta['description'] ?>" class="pin-it-button" count-layout="horizontal">
					<img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" height="17"/>
				</a>
			</p>
		</div>
	</div>
	<div class="clear"></div>
<div>