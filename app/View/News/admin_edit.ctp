<div class="collections form">
    <?php echo $this->Form->create('News'); ?>
    <fieldset>
        <legend><?php echo __('Admin Add News'); ?></legend>
       <?php
        echo $this->Form->input('title' , array('label' => 'Titre :'  , 'required' => 'required'));    
        ?>
		<label for="ArtistBiography"><?php echo __('Brève description'); ?> :</label>
		<?php echo $this->Uploader->tinymce('description'); ?>
		<label for="mediasCollection">Photos :</label><?php
        echo $this->Uploader->iframe('News', $this->request->data['News']['id']) ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>


