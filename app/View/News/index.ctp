<div class="content-new">
	<?php
	if (!empty($news)):
		foreach($news as $new):
	?>
		<div class="row-new">
			<?php if(!empty($new['Media'])): ?>
				<?php
					$temp = true;
					if (count($new['Media']) == 1){
						foreach ($new['Media'] as $media){
							$arr = explode(".", $media['file']);
							if (end($arr) == 'pdf'){
								echo "<a href='/img/" . $media['file'] . "'><img  src='/img/img_pdf.jpg' /></a>";
								$temp = false;
							}
						}
					} elseif (count($new['Media']) == 2) {
						$img = "";
						$pdf = "";
						foreach ($new['Media'] as $media){
							$arr = explode(".", $media['file']);
							if (end($arr) == 'pdf'){
								$pdf = $media['file'];
								$temp = false;
							} else{
								$img =  $media['file'] ;
							}
						}
						if (!$temp ){
							echo "<a  target='_blank' href='/img/" . $pdf . "'><img  src='/img/" . $img . "' /></a>";
						}
					} else {
					 ?>
						<?php $img = ''; 
						if ($temp):
							foreach($news['Media'] as $media): ?>
								<?php $img = $media['file']; echo $this->Html->image($media['file'], array('class' => 'rsImg', 'alt' => '')); ?>
							<?php endforeach;
						endif;
						?>
						<?php $temp = true;
						foreach($new['Media'] as $media): ?>
							<?php if ($temp){ echo $this->Html->image($media['file'], array('class' => 'rsImg', 'alt' => '')); $temp = false; } ?>
						<?php
						endforeach;
					}
				?>
			<?php endif; ?>
			<h3><?php echo $this->Html->link( $new['News']['title'] , array( 'controller' => 'news','action' => 'view', $new['News']['id']),array("class" => "title"));
				 ?></h3>
			<div class="clear"></div>
		</div>
	<?php
		endforeach;
	else: ?>
	<h3>Nouvelles Empty</h3>
	<?php endif; ?>
	<div class="clear"></div>
	<div id="paginateur">
		<?php
		//echo $this->Paginator->first(__('first'));
		echo $this->Paginator->numbers(array(
			'class' => 'paginator',
			'currentClass' => 'paginatorCurrentPage',
			'separator' => ''
		));
		//echo $this->Paginator->last(__('last'));
		?>
	</div>
</div>