<div class="collections index">
    <h2><?php echo __('News'); ?></h2>
   
    <div class="actions">
        <h3><?php echo __('Actions'); ?></h3>
        <ul>
            <li><?php echo $this->Html->link(__('Nouvelles informations'), array('action' => 'add')); ?></li>
        </ul>
    </div>
    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('title' , 'Titre'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php foreach ($news as $new): ?>
            <tr>
                <td><?php echo h($new['News']['id']); ?>&nbsp;</td>
                <td><?php echo h($new['News']['title']); ?>&nbsp;</td>            
                <td class="actions">
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $new['News']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $new['News']['id']), null, __('Are you sure you want to delete # %s?', $new['News']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>

    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
