<div class="collections form">
    <?php echo $this->Form->create('News'); ?>
    <fieldset>
        <legend><?php echo __('Admin Add News'); ?></legend>
       <?php
        echo $this->Form->input('title' , array('label' => 'Titre :'  , 'required' => 'required'));    
        ?>
		<label for="ArtistBiography"><?php echo __('Description'); ?> :</label>
		<?php echo $this->Uploader->tinymce('description'); ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>