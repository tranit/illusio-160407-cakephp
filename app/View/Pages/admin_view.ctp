<div class="pages view">
    <?php //echo debug($page); ?>
    <h2><?php echo __('Page'); ?></h2>
    <div class="actions">
        <h3><?php echo __('Actions'); ?></h3>
        <ul>
            <li><?php echo $this->Html->link(__('Edit Page'), array('action' => 'edit', $page['Page']['id'])); ?> </li>
        </ul>
    </div>
    <table>
        <tr>
            <td><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($page['Page']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <?php foreach (Configure::read('Config.languages') as $lang) { ?>
            <tr>
                <td><?php echo __('Name(' . $lang . '):'); ?></td>
                <td>
                    <?php echo h($page['Page']['name'][$lang]); ?>
                    &nbsp;
                </td>
            </tr>
            <?php if ($page['Page']['id'] == 1 || $page['Page']['id'] == 5 || $page['Page']['id'] == 6) { ?>
                <tr>
                    <td><?php echo __('Description(' . $lang . '):'); ?></td>
                    <td>
                        <?php print($page['Page']['description'][$lang]); ?>
                        &nbsp;
                    </td>
                </tr>
            <?php } ?>

        <?php } ?>

    </table>
    <?php echo $this->Uploader->iframe('Page', $page['Page']['id']); ?>
</div>