<?php
if ($this->Session->read('User.language') == 'fre') {
    $i = 0;
} elseif ($this->Session->read('User.language') == 'eng') {
    $i = 1;
}
?>
<div class="expo">
    <?php
    foreach ($artist_page as $page) {
		if (!empty($page['Media'])):
		?>
			<div class="row-item">
				<?php
					$tem = true;
					foreach($page['Media'] as $media){
						if($tem){
						 list($uploads, $year, $month, $name) = split('[/.-]', $media['file']);
                                $name = str_replace('_', ' ', $name);
							echo $this->Html->image($media['file'], array('class' => 'rsImg', 'alt' => $name));
							$tem = false;
						}
					}
					 ?>
				<h4><?php
				echo $this->Html->link( $page['Artists']['firstname'] . " " .$page['Artists']['lastname'] , array( 'controller' => 'artists','action' => 'view', $page['Artists']['id']),array("class" => "title"));
				 ?></h4>
					<div class="royalSlider">
						<div class="rsOverflow"></div>
					</div>
			</div>
		<?php
		endif;
		}
    ?>
	<div class="clear"></div>

