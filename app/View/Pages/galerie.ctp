<?php
if ($this->Session->read('User.language') == 'fre') {
    $i = 0;
} elseif ($this->Session->read('User.language') == 'eng') {
    $i = 1;
}
?>
<div class="royalSlider rsMinW">
		
<!--    <ul class="bjqs">-->
        <?php
        foreach ($info_page['0']['Media'] as $p) {
            echo $this->Html->image($p['file'], array('class' => 'rsImg'));
            //echo $this->Html->image(sprintf($p['filef'], 200, 300));
            //echo $this->Image->maxDimension($p['file'], '', 500);
            //echo $this->Html->div('img', array('data-src' => $p['file']));
        }
        ?>
	<div class="rsOverflow">
	</div>
	
</div>

<div class="texte">
    <p>
        <?php
        echo $info_page['0']['_description'][$i]['content'];
        ?>
    </p>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$(".aDemInfoFR").css("display","none");
	$(".aDemInfoEN").css("display","none"); 
});
</script>
