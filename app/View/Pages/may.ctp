<div id="lang_page">
    <?php
    echo $this->Html->link('Fr', array('language' => 'fre', 'controller' => 'pages', 'action' => 'home'), array('id' => 'lang_main_fr', 'class' => 'lang_main'));
    echo $this->Html->link('En', array('language' => 'eng', 'controller' => 'pages', 'action' => 'home'), array('id' => 'lang_main_en', 'class' => 'lang_main'));
    ?>
</div>