<div class="pages form">
    <?php echo $this->Form->create('Page'); ?>
    <?php //echo debug($this->request->data['Page']); ?>
    <fieldset>
        <legend><?php echo 'Page '. $this->request->data['Page']['name']['fre']; ?></legend>
        <?php
        foreach (Configure::read('Config.languages') as $lang) {
            echo $this->Form->input('Page.id');
            echo $this->Form->input('Page.name.' . $lang, array('label' => 'Name (' . $lang . ') :'));
            if($this->request->data['Page']['id'] == 1 || $this->request->data['Page']['id'] == 3 || $this->request->data['Page']['id'] == 5 || $this->request->data['Page']['id'] == 6) {
                ?><label for="PageDescription<?php echo $lang; ?>">Description (<?php echo $lang; ?>) :</label><?php
                echo $this->Uploader->tinymce('Page.description.'. $lang); // to get tinymce you have to change "<?" to "<"+"?" in the tiny_mce.js (done)
            }
        }
        ?><label for="mediasPage">Photos :</label><?php
        echo $this->Uploader->iframe('Page', $this->request->data['Page']['id']);
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>