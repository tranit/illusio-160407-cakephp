<?php //echo $this->Html->link($this->Html->image('charles-tassin.jpg'), 'http://www.charlestassin.com', array('escape' => false, 'id' => 'charlestassin')); ?>

<?php
if ($this->Session->read('User.language') == 'fre') {
    $i = 0;
} elseif ($this->Session->read('User.language') == 'eng') {
    $i = 1;
}
?>
<div class="royalSlider rsMinW">
    <?php
    foreach ($info_page['0']['Media'] as $p) {
        echo $this->Html->image($p['file'], array('class' => 'rsImg'));
    }
    ?>
</div>
<div class="texte">
    <p>
        <?php
        echo $info_page['0']['_description'][$i]['content'];
        ?>
    </p>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$(".aDemInfoFR").css("display","none");
	$(".aDemInfoEN").css("display","none"); 
});
</script>